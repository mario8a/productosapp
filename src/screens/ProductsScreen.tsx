import React, { useContext, useEffect } from 'react';
import { Text, View, FlatList, StyleSheet, RefreshControl } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { ProductsContext } from '../context/ProductsContext';
import { StackScreenProps } from '@react-navigation/stack';
import { ProductsStackParams } from '../navigator/ProductsNavigator';

interface Props extends StackScreenProps<ProductsStackParams,'ProductsScreen'>{}

export const ProductsScreen = ({navigation}: Props) => {


  const [refreshing, setRefreshing] = React.useState(false);
  const { products, loadProducts } = useContext(ProductsContext);

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity
          activeOpacity={0.8}
          style={{marginRight: 10}}
          onPress={() => navigation.navigate('ProductScreen', {})}
        >
          <Text>Agregar</Text>
        </TouchableOpacity>
      ),
    });
  }, []);


  const loadProductsFromBackend = async () => {
    setRefreshing(true);
    await loadProducts();
    setRefreshing(false);
  };

  return (
    <View style={{flex: 1, marginHorizontal: 10}}>

      <FlatList
        data={products}
        keyExtractor={(item) => item._id}
        renderItem={({item}) => (
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => navigation.navigate('ProductScreen', {
              id: item._id,
              name: item.nombre,
            })}
          >
            <Text style={styles.productname}>{item.nombre}</Text>
          </TouchableOpacity>
        )}
        ItemSeparatorComponent={(() => <View style={styles.separator}/>)}
        
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={loadProductsFromBackend}
          />
        }
      />

    </View>
  );
};


const styles = StyleSheet.create({
    productname : {
      fontSize: 20,
    },
    separator: {
      borderBottomWidth: 5,
      marginVertical: 5,
      borderBottomColor: '#ccc',
    }
});
