import React, { createContext, useEffect, useReducer } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { LoginData, LoginResponse, RegisterData, Usuario } from '../interface/userInterface';
import { authReducer, AuthState } from './AuthReducer';
import cafeApi from '../api/cafeApi';

type AuthContextProps = {
  errorMessage: string;
  token: string | null;
  user: Usuario | null;
  status: 'checking' | 'authenticated' | 'not-authenticated';
  signUp: (registerData: RegisterData) => void;
  signIn: (loginData: LoginData) => void;
  logOut: () => void;
  removeError: () => void;
}

const authInitialState: AuthState = {
  status: 'checking',
  token: null,
  user: null,
  errorMessage: '',
}

export const AuthContext = createContext({} as AuthContextProps);


export const AuthProvider = ({ children }: any) => {

  const [state, dispatch] = useReducer(authReducer, authInitialState);

  useEffect(() => {
    checkToken();
  }, []);

  const checkToken = async () => {
    const token = await AsyncStorage.getItem('token');
    //token null = No authenticado
    if (!token) return dispatch({ type: 'NOT_AUTHENTICATED' });

    //Hay token
    const resp = await cafeApi.get('/auth');

    if (resp.status !== 200) {
      return dispatch({ type: 'NOT_AUTHENTICATED' });
    }

    await AsyncStorage.setItem('token', resp.data.token);
    dispatch({
      type: 'SIGN_UP',
      payload: {
        token: resp.data.token,
        user: resp.data.usuario,
      },
    });
  };


  const signIn = async({correo, password}: LoginData) => {
    try {
      const resp = await cafeApi.post<LoginResponse>('/auth/login',{correo, password});
      dispatch({
        type: 'SIGN_UP',
        payload: {
          token: resp.data.token,
          user: resp.data.usuario,
        },
      });

      await AsyncStorage.setItem('token', resp.data.token);

    } catch (error) {
      console.log({error});
      dispatch({
        type: 'ADD_ERROR',
        payload: error.response.data.errors[0].msg || 'Informacion invalida',
      });
    }
  };

  //Register user
  const signUp = async ({nombre, correo, password}: RegisterData) => {
    try {
      const resp = await cafeApi.post('/usuarios', {nombre, correo, password});
      dispatch({
        type: 'SIGN_UP',
        payload: {
          token: resp.data.token,
          user: resp.data.usuario,
        },
      });

      await AsyncStorage.setItem('token', resp.data.token);
    } catch (error) {
      console.log({error});
      dispatch({
        type: 'ADD_ERROR',
        payload: error.response.data.errors[0].msg || 'El correo ya esta registrado',
      });
    }
  };

  const logOut = async () => {
    await AsyncStorage.removeItem('token');
    dispatch({ type: 'LOGOUT' });
  };

  const removeError = () => {
    dispatch({
      type: 'REMOVE_ERROR',
    });
  };

  return (
    <AuthContext.Provider value={{
      ...state,
      signUp,
      signIn,
      logOut,
      removeError,
    }}>
      {children}
    </AuthContext.Provider>
  );
};
