import { StackScreenProps } from '@react-navigation/stack';
import React, { useContext, useEffect } from 'react';
import { Text, TouchableOpacity, View, KeyboardAvoidingView, Platform, TextInput, Keyboard, Alert } from 'react-native';
import { WhiteLogo } from '../components/WhiteLogo';
import { AuthContext } from '../context/AuthContext';
import { useForm } from '../hooks/useForm';
import { loginStyles } from '../theme/loginTheme';

interface Props extends StackScreenProps<any,any>{}

export const RegisterScreen = ({navigation}: Props) => {

  const {signUp, errorMessage, removeError} = useContext(AuthContext);

  const {email, password, name, onChange} = useForm({
    email: '',
    password: '',
    name: '',
  });

  useEffect(() => {
    if (errorMessage.length === 0) return;

    Alert.alert(
      'Registro incorrecto',
      errorMessage,
      [{text: 'OK', onPress: removeError}],
    );

  },[errorMessage]);

  const onRegister = () => {
    //Ocultar teclado
    Keyboard.dismiss();
    signUp({correo: email, password, nombre: name});
  };

  return (
    <>

      <KeyboardAvoidingView
        style={{flex: 1, backgroundColor: '#5856D6'}}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      >
        <View style={loginStyles.formContainer}>

          {/* Keyboard avoid view */}
          <WhiteLogo />

          <Text style={loginStyles.title}>Register</Text>

          <Text style={loginStyles.label}>Name</Text>
          <TextInput
            placeholder="Ingrese su nombre"
            placeholderTextColor="rgba(255,255,255,0.4)"
            underlineColorAndroid="white"
            style={[
              loginStyles.inputField,
              (Platform.OS === 'ios') && loginStyles.inputFieldIOS,
            ]}
            selectionColor="white"

            onChangeText={(value) => onChange(value, 'name')}
            value={name}
            onSubmitEditing={onRegister}

            autoCapitalize="words"
            autoCorrect={false}
          />

          <Text style={loginStyles.label}>Email</Text>
          <TextInput
            placeholder="Ingrese su email"
            placeholderTextColor="rgba(255,255,255,0.4)"
            keyboardType="email-address"
            underlineColorAndroid="white"
            style={[
              loginStyles.inputField,
              (Platform.OS === 'ios') && loginStyles.inputFieldIOS,
            ]}
            selectionColor="white"

            onChangeText={(value) => onChange(value, 'email')}
            value={email}
            onSubmitEditing={onRegister}

            autoCapitalize="none"
            autoCorrect={false}
          />

          <Text style={loginStyles.label}>Contraseña</Text>
          <TextInput
            placeholder="*******"
            placeholderTextColor="rgba(255,255,255,0.4)"
            underlineColorAndroid="white"
            secureTextEntry
            style={[
              loginStyles.inputField,
              (Platform.OS === 'ios') && loginStyles.inputFieldIOS,
            ]}
            selectionColor="white"

            onChangeText={(value) => onChange(value, 'password')}
            value={password}
            onSubmitEditing={onRegister}

            autoCapitalize="none"
            autoCorrect={false}
          />

          {/* Boton login */}
          <View style={loginStyles.buttonContainer}>
            <TouchableOpacity
              activeOpacity={0.8}
              style={loginStyles.button}
              onPress={onRegister}
            >
              <Text style={loginStyles.buttonText}>Crear cuenta</Text>
            </TouchableOpacity>
          </View>

          {/* Crear una nueva cuenta */}
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => navigation.replace('LoginScreen')}
              style={{
                position: 'absolute',
                top: 50,
                left: 20,
                borderWidth: 1,
                borderColor: 'white',
                paddingHorizontal: 10,
                paddingVertical: 5,
                borderRadius: 100
              }}
            >
            <Text style={loginStyles.buttonText}>Login</Text>
            </TouchableOpacity>

        </View>
      </KeyboardAvoidingView>
    </>
  );
};
